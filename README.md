
<!-- README.md is generated from README.Rmd. Please edit that file -->

# neuroastro <img src="man/figures/logo.png" align="right" height="139" alt="" />

<!-- badges: start -->
<!-- badges: end -->

neuroastro is an app that allows to upload data collected from
astronauts during their mission days, and to display these data using
profile plots. These plots can then be downloaded to be further
integrated to any report.

## Installation

The app is deployed on Posit Connect. You can directly access it
[here](https://o030r4-pierre-wallet.shinyapps.io/neuroastro/).

Because neuroastro is developed as a traditional package, you can
install the development version of neuroastro like so:

``` r
if(!require(devtools)){install.packages("devtools")}
devtools::install_gitlab("pierrewallet/neuroastro")
```

Once this step achieved, you can locally run the app simply using:

``` r
neuroastro::run_app()
```

For more information please consult its
[documentation](https://neuroastro-pierrewallet-e8cc85400e9f2175bffdec7d7b0836592e99006.gitlab.io).

## Usage

Please note that the file to be uploaded should stick to a few integrity
constraints so that the app works as expected.

- The expected file extension is .csv
- The .csv file should contain 4 mandatory columns:
  - *ID* which represents the astronaut ID
  - *MD* which stands for Mission Day and represents the timepoints
  - *Value* which contains the different registered parameter values
  - *Parameter* which indicates the observed Parameter

## Example

This is an example of the profile plots that you get with the app, based
on a dummy dataset. The left side is the mean profile with the 95%
confidence interval bounds calculated over the different observations,
whereas the right side represents each individual profile.  
The dataset giving the mean calculations is also provided.

``` r
library(neuroastro)
## basic example code
df_test <- data.frame(
  ID = c("A", "A", "A", "B", "B", "B"),
  MD = rep(c("MD015", "MD045", "MD075"),2),
  Value = c(0, 2, 3, 4, 1, 2),
  Parameter = rep(c("PSS-Total", "PSS-Total", "PSS-Total"),2)
)
s <- side_plots(
  df = df_test,
  param = "PSS-Total",
  colour = TRUE
)
s$plot
```

<img src="man/figures/README-example-1.png" width="100%" />

``` r
s$mean_data
#> # A tibble: 3 × 7
#>   `Mission Day` Count  Mean `Standard Deviation` `Standard Error`
#>   <chr>         <int> <dbl>                <dbl>            <dbl>
#> 1 MD015             2   2                  2.83               2  
#> 2 MD045             2   1.5                0.707              0.5
#> 3 MD075             2   2.5                0.707              0.5
#> # ℹ 2 more variables: `95% CI Lower Bound` <dbl>, `95% CI Upper Bound` <dbl>
```

## Contributing

In case you want to report a bug, propose a new feature/function please
raise an [issue](https://gitlab.com/pierrewallet/neuroastro/-/issues).

## Authors, acknowledgment and attribution

I would personally like to thank Jean Pauly, main sponsor of this
project. His trust allowed me to convert my theoretical shiny knowledge
into a practical use-case.

The neuroastro logo is built upon an astronaut icon coming from [The
noun project](https://thenounproject.com/icon/astronaut-1616022/) and
made by Rodrigo Leles, licensed under [CC by
3.0](https://creativecommons.org/licenses/by/3.0/).

## License

This project is under MIT licence.
