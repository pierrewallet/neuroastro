library(magick)
system.file("astropic2.png", package = "neuroastro")
astro <- image_read(path = system.file("astropic2.png", package = "neuroastro"))
print(astro)

astro2 <- image_crop(astro, "700x600")
print(astro2)

#resize acc to height
astro3 <- image_scale(astro2, "x650")
astro4 <- image_background(astro3, color = "white")

# create a graph to add as a layer on the image above
df <- data.frame(
  MD = c("015", "045", "075", "105"),
  ID = rep(c("A", "B", "C"), each = 4),
  Value = c(5, 4, 6, 4, 3, 6, 4, 5, 4, 5, 3, 5)
  )

library(ggplot2)
g <- ggplot(data = df, aes(x = MD, y = Value, group = ID, color = ID)) +
  geom_line(linewidth = 9) +
  geom_point(size = 10) +
  theme_classic() + 
  ylim(2,7)

ggsave(filename = paste(system.file(package = "neuroastro"), "graph.png", sep="/"), g)

graph <- image_read(path = system.file("graph.png", package = "neuroastro"))
print(graph)

graph2 <- image_scale(graph, "600x600")
print(graph2)
graph3 <- image_crop(graph2, "450x250+50+80")
print(graph3)
graph4 <- image_scale(graph3, "350x350")
graph5 <- image_transparent(graph4, 'white')

final <- image_composite(astro4, graph5, offset = "+120+200")

image_write(final, path = paste(system.file(package = "neuroastro"), "prelogo.png", sep="/"), format = "png")

# Finalize the hex logo with https://connect.thinkr.fr/hexmake/
# blue color code is 26 73 168
